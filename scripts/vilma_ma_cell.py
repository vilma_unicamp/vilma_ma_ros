#!/usr/bin/python
# copyright 2015  Olmer Garcia  olmerg@gmail.com
 # Licensed under the Apache License, Version 2.0 (the "License");
 # you may not use this file except in compliance with the License.
 # You may obtain a copy of the License at
 #
 #     http://www.apache.org/licenses/LICENSE-2.0
 #
 # Unless required by applicable law or agreed to in writing, software
 # distributed under the License is distributed on an "AS IS" BASIS,
 # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 # See the License for the specific language governing permissions and
 # limitations under the License.
import math
from datetime import datetime
import time
from obd_utils import scanSerial
import rospy
import json
import urllib2
from sensor_msgs.msg import Imu, NavSatFix, NavSatStatus
from std_msgs.msg import Float64MultiArray, MultiArrayDimension, String

if __name__ == '__main__': 
	rospy.init_node('cell_driver', anonymous=True)
	pub_cell = rospy.Publisher('motog/all', String, queue_size=1)
	pub_navsatfix = rospy.Publisher('motog/fix', NavSatFix, queue_size=1)
	navsat = NavSatFix()
	link= 'http://192.168.140.8:9000/algo.esp?acc=1&orientation=1&compass=1&gps=1'
	time_out=1
	rate = rospy.Rate(time_out)
	while not rospy.is_shutdown():
				
		try:
			f = urllib2.urlopen(link,timeout=time_out)
			myfile = f.read()
			f.close()
			#print myfile
			data=json.loads(myfile);
			#print data['location']
			pub_cell.publish(myfile)
		# from https://github.com/ros-drivers/novatel_span_driver/blob/master/novatel_span_driver/src/novatel_span_driver/publisher.py
		# TODO: The timestamp here should come from SPAN, not the ROS system time.
			navsat.header.stamp = rospy.Time.now()
			navsat.status.service = NavSatStatus.SERVICE_GPS
			navsat.latitude = data["location"]["network"]["latitude"]
			navsat.longitude = data["location"]["network"]["longitude"]
        # Altitude in metres.
			navsat.altitude = -1;#data["location"]["network"]["accuracy"]
			navsat.position_covariance[0] = pow(2, data["location"]["network"]["accuracy"] )
			navsat.position_covariance[4] = pow(2, data["location"]["network"]["accuracy"])
			#navsat.position_covariance[8] = pow(2, data["location"]["network"]["accuracy"])
			navsat.position_covariance_type = NavSatFix.COVARIANCE_TYPE_DIAGONAL_KNOWN
			pub_navsatfix.publish(navsat);
		except IOError,e:
			#print "Cell phone "+str(e)
			rospy.loginfo("Cell phone "+str(e))
			#print e

		
		
		rate.sleep()