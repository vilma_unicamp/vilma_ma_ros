#!/usr/bin/python
# -*- coding: utf-8 -*-

import time

# copyright 2015  Olmer Garcia  olmerg@gmail.com
 # Licensed under the Apache License, Version 2.0 (the "License");
 # you may not use this file except in compliance with the License.
 # You may obtain a copy of the License at
 #
 #     http://www.apache.org/licenses/LICENSE-2.0
 #
 # Unless required by applicable law or agreed to in writing, software
 # distributed under the License is distributed on an "AS IS" BASIS,
 # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 # See the License for the specific language governing permissions and
 # limitations under the License.



import os
from collections import deque
from std_msgs.msg import String
from std_msgs.msg import Float64MultiArray, MultiArrayDimension
from sensor_msgs.msg import Joy
import rospy

worksheet=[]
joystick_ma=[]
log_list=[]
last_time=-1
timer_time=0.02
reboot_counter=0




		

def joystick_callback(data):
	global joystick_ma
	joystick_ma[0]=rospy.get_time()
	joystick_ma[1]=50  #50 millisecond is the valididty of the msg
	if data.buttons[0]==1:
		if data.axes[4]<-0.25:
			joystick_ma[6]=2
			joystick_ma[7]=abs(data.axes[4])/4
			joystick_ma[2]=2
			joystick_ma[3]=0
		elif data.axes[4]>0.25:
			joystick_ma[2]=2
			joystick_ma[3]=abs(data.axes[4])
			joystick_ma[6]=2
			joystick_ma[7]=0
		else:
			joystick_ma[6]=2
			joystick_ma[7]=0
			joystick_ma[2]=2
			joystick_ma[3]=0
	   
		if abs(data.axes[5])>0.25:
			joystick_ma[4]=1
			joystick_ma[5]=data.axes[5]
		else:
			joystick_ma[4]=1
			joystick_ma[5]=0
	
	else:
		joystick_ma[2]=0
		joystick_ma[3]=0
		joystick_ma[4]=0
		joystick_ma[5]=0
		joystick_ma[6]=0
		joystick_ma[7]=0
	
		
	
		
	
	#print sensors_ma
	

def timer_callback(event):
	global joystick_ma
	mytime=rospy.get_time()
	if(joystick_ma[0]+joystick_ma[1]<mytime):
		joystick_ma[0]=mytime
		joystick_ma[2]=0
		joystick_ma[3]=0
		joystick_ma[4]=0
		joystick_ma[5]=0
		joystick_ma[6]=0
		joystick_ma[7]=0
	data_obd=Float64MultiArray()
	dim_odb = MultiArrayDimension()
	dim_odb.size = len(joystick_ma)
	dim_odb.label = "time PPC	time Validity	Brake command	Brake Value	Steer command	Steer value	Gas command	Gas Value	Gear state	Gear value"
	dim_odb.stride = dim_odb.size
	data_obd.layout.dim.append(dim_odb)
	data_obd.layout.data_offset = 0
	data_obd.data=joystick_ma
	pub_obd.publish(data_obd)
	


def init_data():
	global joystick_ma
	for i in range(0,10):
		joystick_ma.append(0)
	joystick_ma[0]=rospy.get_time()
	joystick_ma[1]=50  #50 millisecond is the valididty of the msg
	

if __name__ == '__main__':

	rospy.init_node('joystick', anonymous=True)
	init_data()
	rospy.Subscriber("spacenav/joy", Joy, joystick_callback)
	pub_obd = rospy.Publisher('/vilma_ma_ros/joystick_ma', Float64MultiArray, queue_size=1)
	
	rospy.Timer(rospy.Duration(timer_time), timer_callback)
	rospy.spin()
	#worksheet.appendrow

# Select a range
#i=3
#range="A"+str(i)+":D"+str(i)
#cell_list = worksheet.range(range)

#for cell in cell_list:
#    cell.value = 'O_o'

# Update in batch
#worksheet.update_cells(cell_list)
