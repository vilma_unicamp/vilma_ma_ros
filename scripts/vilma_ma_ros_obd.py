#!/usr/bin/python
# copyright 2015  Olmer Garcia  olmerg@gmail.com
#to run like root is necessary to add to group of dialup
#sudo adduser root dialout
# this file is taken from pyODB https://github.com/roflson/pyobd and modified
# Copyright 2004 Donour Sizemore (donour@uchicago.edu)
# Copyright 2009 Secons Ltd. (www.obdtester.com)
#
# This file is part of pyOBD.
#
# pyOBD is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# vilma_ma_OBD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with vilma_ma_OBD ; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
###########################################################################
import obd_io
import serial
import platform
import obd_sensors
import math
from datetime import datetime
import time
from obd_utils import scanSerial
import rospy
#from std_msgs.msg import String
from std_msgs.msg import Float64MultiArray, MultiArrayDimension

class OBD_Recorder():
	def __init__(self, log_items):
		self.port = None
		self.log_items=log_items
		self.sensorlist = []
		for item in log_items:
			self.add_log_item(item)
		self.length=len(log_items)+2
		#fiat ponto 1.4 8V Flex attractive 2011 gear ratios   [1,2,3,4,5,reverse]
		self.gear_ratios = [4.273, 2.238, 1.444, 1.029, 0.0838, -3.909]
		
	def header(self):
		log_item="Time"
		for item in self.log_items:
			log_item=log_item+","+item
		log_item=log_item+","+"gear"
		return log_item
	
	def close(self):
		if(self.port):
			self.port.close()
		self.port = None
		
	def connect(self):
        #portnames = scanSerial()
        #print portnames
		portnames = ['/dev/ttyUSB0']
        #
		for port in portnames:
			self.port = obd_io.OBDPort(port, None, 2, 2)
			if(self.port.State == 0):
				self.port.close()
				self.port = None
			else:
				break
		if(self.port):
			print "Connected to "+self.port.port.name
            
	def is_connected(self): 
		return self.port
        
	def add_log_item(self, item):
		for index, e in enumerate(obd_sensors.SENSORS):
			if(item == e.shortname):
				self.sensorlist.append(index)
				#print "Logging item: "+e.name
				break
				
	def record_data(self):
		if(self.port is None):
			return None;
		localtime = time.time()
        #current_time = str(localtime.hour)+":"+str(localtime.minute)+":"+str(localtime.second)+"."+str(localtime.microsecond)
        #log_string = current_time
		log_vector=[]
		log_vector.append(localtime)
		if(self.port is None):
			#tmp=[float('nan')] * (self.length-1)
			#log_vector.extend(tmp)
			return log_vector
		results = {}
		#TODO:remove results is not necessary
		for index in self.sensorlist:
			(name, value, unit) = self.port.sensor(index)
            #log_string = log_string + ","+str(value)
			log_vector.append(value)
			results[obd_sensors.SENSORS[index].shortname] = value;
		gear = self.calculate_gear(results["rpm"], results["speed"])
		log_vector.append(gear)
		#log_string = log_string + "," + str(gear)
		return log_vector
            
	def calculate_gear(self, rpm, speed):
		if speed == "" or speed == 0:
			return 0
		if rpm == "" or rpm == 0:
			return 0
		rps = float(rpm)/60
		mps = (float(speed)*1000)/3600
        #fiat ponto 1.4 8V Flex attractive 2011 differential 
		primary_gear = 66/15 
		final_drive  = 1/1
        #fiat ponto 1.4 8V Flex attractive 2011 differential diameter R15=0.381
		tyre_circumference = math.pi*0.381 #meters
		current_gear_ratio = (rps*tyre_circumference)/(mps*primary_gear*final_drive)
        #print current_gear_ratio
		gear = min((abs(current_gear_ratio - i), i) for i in self.gear_ratios)[1] 
		return gear
class log_Recorder():
	def __init__(self,path,log_items):
		localtime = time.localtime(time.time())
		filename = path+"vilma-"+str(localtime[0])+"-"+str(localtime[1])+"-"+str(localtime[2])+"-"+str(localtime[3])+"-"+str(localtime[4])+"-"+str(localtime[5])+".log"
		#print filename
		self.log_file = open(filename, "w")
		
		#self.add_line(self.header(log_items));
		#self.log_file.close()

	def add_line(self,log_string):
		try:
			self.log_file.write(log_string+"\n")
			return true
		except:
			return None
	def add_vector(self,data):
		if dato:
			tmp=[str(element)for element in data]
			#print data
			self.add_line(",".join(tmp))
		else:
			self.add_line(str(datetime.now()))
	def __del__(self):
		self.log_file.close()
		
if __name__ == '__main__':            
	logitems = ["rpm", "speed", "throttle_pos", "load"]
	#logitems =[i.shortname for i in obd_sensors.SENSORS]
	rospy.init_node('odb_driver', anonymous=True)
	time.sleep( 10 )
	o = OBD_Recorder(logitems)
	o.connect()
	#while not o.is_connected():
		
		
	#else:
	if 1:
		#log=log_Recorder('/home/robot/odb/',logitems)
		#log.add_line(o.header())
		pub_obd = rospy.Publisher('odb_data', Float64MultiArray, queue_size=1)
		data_obd=Float64MultiArray()
		dim_odb = MultiArrayDimension()
		dim_odb.size = len(logitems)+2
		dim_odb.label = o.header()
		dim_odb.stride = dim_odb.size
		data_obd.layout.dim.append(dim_odb)
		data_obd.layout.data_offset = 0
		rospy.loginfo("publishing odbII data:"+o.header())
	
		rate = rospy.Rate(1)
		dato=[]
		i=0
		while not rospy.is_shutdown():
			if o.is_connected() is None:
				
				i=i+1
			if i==30:
				i=0
				o.connect()
				if o.is_connected() is None:
					rospy.logerr("Cannot connect to OBDII interface waiting 30s")
			
			dato=o.record_data()
			#replace obd time by rospy.get_time()
			
			if dato:
				
				dato[0]=rospy.get_time();
				#print dato
				data_obd.data=dato
				pub_obd.publish(data_obd)
				#if NODATA in the vector exit of the program 
			#else:
				#log.add_line("Error with the obd cable")
				#@bug it do not detect that is disconnected
				#i=i+1
				#rospy.logerr("Error with the obd cable")
				
				
			rate.sleep()
	o.close()	

