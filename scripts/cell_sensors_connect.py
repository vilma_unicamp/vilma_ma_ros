#!/usr/bin/python
# -*- coding: utf-8 -*-

import time

# copyright 2015  Olmer Garcia  olmerg@gmail.com
 # Licensed under the Apache License, Version 2.0 (the "License");
 # you may not use this file except in compliance with the License.
 # You may obtain a copy of the License at
 #
 #     http://www.apache.org/licenses/LICENSE-2.0
 #
 # Unless required by applicable law or agreed to in writing, software
 # distributed under the License is distributed on an "AS IS" BASIS,
 # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 # See the License for the specific language governing permissions and
 # limitations under the License.



import os
from collections import deque
from std_msgs.msg import String
from std_msgs.msg import Float64MultiArray, MultiArrayDimension
from sensor_msgs.msg import Imu
import rospy
import math
import tf

worksheet=[]
sensors_pc=[]
log_list=[]
last_time=-1
timer_time=0.02
reboot_counter=0




		

def imu_callback(msg):
	global sensors_pc
	sensors_array=sensors_pc.data
	
	sensors_array[0]=rospy.get_time()
	sensors_array[1]=sensors_array[1]+1 
	(r, p, y) = tf.transformations.euler_from_quaternion([msg.orientation.x, msg.orientation.y, msg.orientation.z, msg.orientation.w])
	sensors_array[2]=r*180/math.pi
	sensors_array[3]=p*180/math.pi
	sensors_array[4]=y*180/math.pi
	print sensors_array[4]
	sensors_pc.data=sensors_array
	pub_obd.publish(sensors_pc)
	


def init_data():
	global sensors_pc
	sensor_array=[]
	for i in range(0,10):
		sensor_array.append(0)
	sensor_array[0]=rospy.get_time() #time
	sensor_array[1]=0  #counter
	sensors_pc=Float64MultiArray()
	dim_odb = MultiArrayDimension()
	dim_odb.size = len(sensor_array)
	dim_odb.label = "time PPC	time Validity	Brake command	Brake Value	Steer command	Steer value	Gas command	Gas Value	Gear state	Gear value"
	dim_odb.stride = dim_odb.size
	sensors_pc.layout.dim.append(dim_odb)
	sensors_pc.layout.data_offset = 0
	sensors_pc.data=sensor_array

	

if __name__ == '__main__':

	rospy.init_node('joystick', anonymous=True)
	init_data()
	rospy.Subscriber("phone1/android/imu", Imu, imu_callback)
	pub_obd = rospy.Publisher('/vilma_ma_ros/sensors_pc', Float64MultiArray, queue_size=1)
	
	rospy.spin()
