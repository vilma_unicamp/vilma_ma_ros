#!/usr/bin/python
# -*- coding: utf-8 -*-


"""
Created on Mon Oct 19 23:19:30 2015
@author: olmerg
copyright 2015  Olmer Garcia  olmerg@gmail.com
Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

"""

import socket
import math
import time
import json
import rospy
import sys
import os
from sensor_msgs.msg import Imu, NavSatFix, NavSatStatus
from geometry_msgs.msg   import TwistWithCovarianceStamped
from std_msgs.msg import Float64MultiArray, MultiArrayDimension, String
from ublox_msgs.msg import NavPOSLLH, NavVELNED
import message_filters
import Queue
from geodesy import utm




sensors_pc=[]
planned_path=[]
_queue=Queue.Queue(10) #maximum number of msg
#syncronize_gps=0



#def gpsdata(gps_data):
#    global syncronize_gps
#    global sensors_pc
#    utm_data=utm.fromLatLong(gps_data.lat*1e-7,gps_data.lon*1e-7, gps_data.height/1000)
#    degree=180*math.atan2(utm_data.easting-float(sensors_pc.data[7]),utm_data.northing-sensors_pc.data[6])/math.pi
#    speed=math.sqrt((utm_data.easting-sensors_pc.data[7])**2+(utm_data.northing-sensors_pc.data[6])**2)
#    print( "E: %.2f,%2f" % (degree,speed))
#    sensors_pc.data[6]= utm_data.northing
#    sensors_pc.data[7]= utm_data.easting
#    if syncronize_gps==1:
#        syncronize_gps=0
#        sensors_pc.data[5]= sensors_pc.data[5]+1
#    elif syncronize_gps==0: 
#	syncronize_gps=2

    #print gps_data.lon*1e-7,gps_data.lat*1e-7,gps_data.hMSL/1000.0    
    #print( "%.2f,%2f " % (sensors_pc.data[6],sensors_pc.data[7]),utm_data.gridZone())


#def gpsvel(gps_vel):
#    global syncronize_gps
#    global sensors_pc
#    angle=gps_vel.heading*1e-5
#    if angle>180:
#	angle=angle-360
	
#    sensors_pc.data[8]= angle
#    sensors_pc.data[9]= gps_vel.gSpeed/100.0
    
#    if syncronize_gps==2:
#        syncronize_gps=0
#        sensors_pc.data[5]= sensors_pc.data[5]+1
#    elif syncronize_gps==0: 
#	syncronize_gps=1

#    print("M %.2f,%.2f ,%.2f" % (sensors_pc.data[8],sensors_pc.data[9],180*math.atan2(gps_vel.velE,gps_vel.velN)/math.pi))#,gps_data.hMSL/1000.0

def cellspeak(msg):
	addmsgtocell(msg.data)


def addmsgtocell(texto):
	if  _queue.full():
            clean=_queue.get()
	    rospy.loginfo("cell speak queue is full: not talked :%s",clean)
        _queue.put(texto)

def gpsall(gps_data,gps_vel):
	utm_data=utm.fromLatLong(gps_data.latitude,gps_data.longitude,gps_data.altitude)
	vel=math.sqrt(gps_vel.twist.twist.linear.y**2+gps_vel.twist.twist.linear.x**2) 
	angle=180*math.atan2(gps_vel.twist.twist.linear.y,gps_vel.twist.twist.linear.x)/math.pi
	#test to review the consistency of the data the period is one , so the speed is calculated, and the angle is estimated
	#degree=180*math.atan2(utm_data.easting-float(sensors_pc.data[7]),utm_data.northing-sensors_pc.data[6])/math.pi
	#speed=math.sqrt((utm_data.easting-sensors_pc.data[7])**2+(utm_data.northing-sensors_pc.data[6])**2)
	#print("T %.2f,%.2f,%.2f,%.2f E %.2f %.2f" % (utm_data.northing,utm_data.easting,angle,vel,degree,speed))
	sensors_pc.data[5]= sensors_pc.data[5]+1	
	sensors_pc.data[6]= utm_data.northing
	sensors_pc.data[7]= utm_data.easting
	sensors_pc.data[8]= angle
	sensors_pc.data[9]= vel



def init_data():
	global sensors_pc
	sensor_array=[]
	for i in range(0,10):
		sensor_array.append(0)
	sensor_array[0]=rospy.get_time() #time
	sensor_array[1]=0  #counter
	sensor_array[5]=0  #counter
	sensors_pc=Float64MultiArray()
	dim_odb = MultiArrayDimension()
	dim_odb.size = len(sensor_array)
	dim_odb.label = "timePC counter yaw picth roll count_gps x y theta vel"
	dim_odb.stride = dim_odb.size
	sensors_pc.layout.dim.append(dim_odb)
	sensors_pc.layout.data_offset = 0
	sensors_pc.data=sensor_array


if __name__ == '__main__':
# Create a TCP/IP socket
    global planned_path
    rospy.init_node('cell_driver', anonymous=True)
#    rospy.Subscriber('/vilma_ublox/navposllh', NavPOSLLH,gpsdata)
#    rospy.Subscriber('/vilma_ublox/navvelned', NavVELNED,gpsvel)
    f = os.popen('ifconfig eth0 | grep "inet\ addr" | cut -d: -f2 | cut -d" " -f1')
    pc_ip=f.read()
    #pc_ip=rospy.get_param('pc_ip', pc_ip)

    pc_port=rospy.get_param('pc_cell_server_port', 10000)
    #pc_ip='143.106.207.248'
    rospy.Subscriber('/cell_speak',String, cellspeak)
    #@bug it can not be syncronized by time because the msgs do not have header :(
    gps_sub = message_filters.Subscriber('/vilma_ublox/fix', NavSatFix)
    velgps_sub = message_filters.Subscriber('/vilma_ublox/fix_velocity', TwistWithCovarianceStamped)
    ts = message_filters.TimeSynchronizer([gps_sub, velgps_sub], 1)
    ts.registerCallback(gpsall)
    pub_cell = rospy.Publisher('vilma_ma_ros/sensors_pc', Float64MultiArray, queue_size=1)
    planned_path = rospy.Publisher('vilma_ma_ros/planned_path', Float64MultiArray, queue_size=1)
    init_data()
    pub_navsatfix = rospy.Publisher('cell/fix', NavSatFix, queue_size=1)
    navsat = NavSatFix()
    navsat.header.seq=0
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# Bind the socket to the port
    server_address = (pc_ip, pc_port)
    rospy.loginfo('Starting up on %s port %s' % server_address)
    sock.bind(server_address)
    _counter=-1
    _counter_gps=0
    adress=''
    sock.settimeout(1)
    #rate = rospy.Rate(time_out)
    while not rospy.is_shutdown():
        
        try:    
            data, address = sock.recvfrom(2048)
            #print ('received %s bytes from %s' % (len(data), address))
	    try:
            	mydata = json.loads(data)
		#print mydata
            	#TODO: it is better to test if _counter+1==mydata['counter']  pero eso no sirve despues de u¿ocurrido un error
            	if _counter!=mydata['c++'] and mydata['orientation'][3]==sum(mydata['orientation'][0:3]):
                	#print (time.time(),[int(x * 180/math.pi) for x in mydata['orientation'][0:3]] )
			sensors_pc.data[0]=mydata['t']
                	sensors_pc.data[1]=sensors_pc.data[1]+1
			sensors_pc.data[2:5]=[float(x * 0.0180/math.pi) for x in mydata['orientation'][0:3]]
			pub_cell.publish(sensors_pc)
			_counter_gps=sensors_pc.data[5]
                	_counter=mydata['c++']
		
			if(mydata.has_key("gps")):
				navsat.header.stamp = rospy.Time.now()
				navsat.header.seq = navsat.header.seq+1
				navsat.status.service = NavSatStatus.SERVICE_GPS
				#print mydata["gps"]
				if mydata["gps"]["gps"] is not None:
					navsat.latitude = mydata["gps"]["gps"]["latitude"]
					navsat.longitude = mydata["gps"]["gps"]["longitude"]
			        # Altitude in metres.
                        	#print (mydata["gps"])
					navsat.altitude =mydata["gps"]["gps"]["altitude"]
					navsat.position_covariance[0] = pow(2, mydata["gps"]["gps"]["accuracy"] )
					navsat.position_covariance[4] = pow(2, mydata["gps"]["gps"]["accuracy"])
					navsat.position_covariance_type = NavSatFix.COVARIANCE_TYPE_DIAGONAL_KNOWN
					pub_navsatfix.publish(navsat);
		else:
			rospy.loginfo('some error in the data received in vilma_qpython')
	    except ValueError:
		rospy.loginfo('exception in the data received in vilma_qpython')	
            #if _counter%100==0:
             #   sent = sock.sendto('ok', address)
	    if _queue.qsize()>0:
                sent = sock.sendto(str(_queue.get()), address)
		#print(_counter)
        except socket.timeout:
           #rospy.loginfo('timeout in the cell phone server')
	   if     _counter_gps!=sensors_pc.data[5]:
		pub_cell.publish(sensors_pc)
		_counter_gps=sensors_pc.data[5]
	
            
    sent = sock.sendto('exit', address)
    time.sleep(0.1)
    sock.close()        
    
    #if data:
    #    sent = sock.sendto(data, address)
    #    print >>sys.stderr, 'sent %s bytes back to %s' % (sent, address)
