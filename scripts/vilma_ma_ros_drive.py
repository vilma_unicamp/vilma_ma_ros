#!/usr/bin/python
# -*- coding: utf-8 -*-
# opcao 1 https://www.mattcutts.com/blog/write-google-spreadsheet-from-python/
#versao 2 https://github.com/burnash/gspread
# pip install gspread
# pip install --upgrade oauth2client
# pip install PyOpenSSL
# pip install pycrypto
import time
#time.strftime('%Y/%m/%d/m.%H:%M:%S')
# set time of the system over http 
# sudo date -s "$(curl -sD - google.com | grep '^Date:' | cut -d' ' -f3-6)Z"
#
#
#netstat -sve --udp
#TODO: make the deque of log limited with maxlen https://docs.python.org/2/library/collections.html#collections.deque
# to get make sudo reboot without password I change the permission of reboot command with visudo http://askubuntu.com/questions/168879/shutdown-from-terminal-without-entering-password
# i think that the performance of this program is really bad may be try the timer of the python and make rosone to get the vilma_state and forget the log
#from threading import Timer
#import time
#def timeout():
#    print "Game over"
#t = Timer(20 * 60, timeout)
# t.start()
# do something else, such as
#time.sleep(1500)
# copyright 2015  Olmer Garcia  olmerg@gmail.com
 # Licensed under the Apache License, Version 2.0 (the "License");
 # you may not use this file except in compliance with the License.
 # You may obtain a copy of the License at
 #
 #     http://www.apache.org/licenses/LICENSE-2.0
 #
 # Unless required by applicable law or agreed to in writing, software
 # distributed under the License is distributed on an "AS IS" BASIS,
 # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 # See the License for the specific language governing permissions and
 # limitations under the License.


import gspread
import json
import os
from oauth2client.client import SignedJwtAssertionCredentials
from collections import deque
from std_msgs.msg import String
from std_msgs.msg import Float64MultiArray
from rosgraph_msgs.msg import Log
import rospy

worksheet=[]
sensors_ma=[]
log_list=[]
last_time=-1
timer_time=120
reboot_counter=0

def log_callback(data):
	#print rospy.get_caller_id(), data.msg
	log_list.append(data.msg)

def sensors_ma_callback(data):
	for i in range(0,13):
		sensors_ma[i]=data.data[i]
		

def sensors_brake_callback(data):
	for i in range(0,13):
		sensors_ma[i+13]=data.data[i]
	#print sensors_ma
	

def timer_callback(event):
	#print 'Timer called at ' + str(event.current_real)
	a=";"
	connected=False
	global last_time
	if(len(log_list)>0):
		a=a.join(list(log_list))
		log_list.clear()
	tmp2=0
	if last_time==-1:
		tmp2=0
		last_time=sensors_ma[1]
	else:
		tmp2=abs(sensors_ma[1]-last_time)
		last_time=sensors_ma[1]
	#localtime = datetime.now()
	#current_time = str(localtime.hour)+":"+str(localtime.minute)+":"+str(localtime.second)+"."+str(localtime.microsecond)
	if tmp2<1.5*timer_time:
		#b=";"
		#append_data((str(rospy.get_time()),str(sensors_ma[0]),str(sensors_ma[1]),str(b.join(sensors_ma)),a))
		b=['',str(rospy.get_time())]
		b.extend([str(element)for element in sensors_ma])
		b.append(a)
		connected=append_data(b)
		
		
	else:
		f = os.popen("netstat -sve --udp")
		now = f.read()
		connected=append_data((str(rospy.get_time()),str(sensors_ma[0]),str(sensors_ma[1]),str(sensors_ma[2]),a,now))
	if connected:
		tmp2=float(worksheet.acell('C1').value)
		#print str(tmp2)
		if tmp2>reboot_counter:
			rospy.logerr("reboot the system bye bye")
			os.popen("sudo reboot")
		

		
def append_data(list):
		try:	
			worksheet.append_row(list)
			return True
		except:
			rospy.logerr("Append error "+str(rospy.get_time()))
			return connect_drive()
	
def connect_drive():
	global worksheet
	f = os.popen("curl -sD - google.com | grep '^Date:' | cut -d' ' -f3-6")
	now = f.read()
	print "date of google ",now

	#os.system('sudo date -s "$(curl -sD - google.com | grep '^Date:' | cut -d' ' -f3-6)Z"')
	json_key = json.load(open('/home/robot/vilma-log-f59674c366b3.json'))
	scope = ['https://spreadsheets.google.com/feeds']
	credentials = SignedJwtAssertionCredentials(json_key['client_email'], json_key['private_key'], scope)
	try:
		gc = gspread.authorize(credentials)
		worksheet = gc.open("datos_vilma_ma_ros2").sheet1
		return True
	except:
		rospy.logerr("connect to google drive error "+str(rospy.get_time()))
		worksheet=[]
		return False

def init_data():
	global sensors_ma
	global log_list
	global reboot_counter
	for i in range(0,26):
		sensors_ma.append(0)
	log_list=deque([])
	reboot_counter=float(worksheet.acell('C1').value)
	
	

if __name__ == '__main__':
	connect_drive()
	init_data()
# Or
	
	rospy.init_node('listener', anonymous=True)
	rospy.Subscriber("rosout", Log, log_callback)
	rospy.Subscriber("vilma_ma_ros/sensors_ma", Float64MultiArray, sensors_ma_callback)
	rospy.Subscriber("vilma_ma_ros/sensors_brake", Float64MultiArray, sensors_brake_callback)
	rospy.Timer(rospy.Duration(timer_time), timer_callback)
	worksheet.update_cell(2, 1, str(rospy.get_time()))
	rospy.spin()
	#worksheet.appendrow

# Select a range
#i=3
#range="A"+str(i)+":D"+str(i)
#cell_list = worksheet.range(range)

#for cell in cell_list:
#    cell.value = 'O_o'

# Update in batch
#worksheet.update_cells(cell_list)