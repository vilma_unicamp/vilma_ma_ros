from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

d = generate_distutils_setup(
    packages=['vilma_ma_ros'],
    scripts=['scripts/vilma_ma_cell.py','scripts/vilma_ma_ros_obd.py','scripts/vilma_ma_ros_drive.py','scripts/vilma_qpython_cell.py'],
    package_dir={'': 'scripts'}
)

setup(**d)
