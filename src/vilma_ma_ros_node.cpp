/**
 *
	 @brief This is main program of the package VILMA_MA
	 //for configure the start up of the program http://docs.ros.org/api/robot_upstart/html/
	 //rosrun robot_upstart install vilma_ma_ros/launch/base.launch
	 // export ROS_MASTER_URI=http://192.168.140.4:11311/
	 //usage install
	 usage: install [-h] [--job JOB] [--interface ethN] [--user NAME]
               [--setup path/to/setup.bash] [--rosdistro DISTRO]
               [--master http://MASTER:11311] [--logdir path/to/logs]
               [--augment]
               pkg/path
	 //rosrun robot_upstart install vilma_ma_ros/launch/vilma_ma_root.launch --master http://VILMA-PC:11311 --interface eth0 
	 //$ sudo service vilma start
	//$ sudo service vilma stop
	 
	 for the scheduling fo the thread could be change after compile make this in the executable:
	 cd devel/lib/vilma_ma_ros     # cd to the directory with your node
	sudo chown root:root vilma_ma_ros_node # change ownship to root
	sudo chmod a+rx vilma_ma_ros_node      # set as executable by all
	sudo chmod u+s vilma_ma_ros_node       # set the setuid bit
	
	para averiguar que valores de prioridad pueden tener las thread  chrt -m
	
	http://answers.ros.org/question/165246/launch-node-with-root-permissions/
	@TODO add image support to the car http://wiki.ros.org/axis_camera
	https://siddhantahuja.wordpress.com/2011/07/20/working-with-ros-and-opencv-draft/
	http://www.transistor.io/revisiting-lane-detection-using-opencv.html
	https://github.com/jdorweiler/lane-detection/blob/master/LaneDetect.cpp
	http://wiki.ros.org/vision_opencv
	//restart eth0 interface  when the controldesk generate the problem!!!!!
	//sudo ifdown --exclude=lo -a && sudo ifup --exclude=lo -a
 @author olmer Garcia olmerg@gmail.com
 * Copyright 2013 Olmer Garcia Bedoya
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#include "ros/ros.h"
#include "std_msgs/Float64MultiArray.h"
#include "ros/callback_queue.h"
#include "ros/advertise_options.h"
#include "ros/subscribe_options.h"
#include "maudp.h"
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/asio/deadline_timer.hpp>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <sched.h>
#include <pthread.h>
#include<sys/time.h>
#include<sys/resource.h>
#include <vector>
#include <queue>  

class vilma_ma_ros{
	std_msgs::Float64MultiArray joystick_ma;
	std_msgs::Float64MultiArray states_ma;
	std_msgs::Float64MultiArray sensors_ma;
	std_msgs::Float64MultiArray sensors_brake;
	 std::queue<std_msgs::Float64MultiArray> joystick_ma_queue;
	std::queue<std_msgs::Float64MultiArray> sensors_pc_queue;
	std_msgs::Float64MultiArray sensors_pc;
	std::queue<std_msgs::Float64MultiArray> planned_path_queue;
	std_msgs::Float64MultiArray planned_path;
	int ma_inputs;
	int ma_outputs;
	int timercounter;
	double operation_mode;
	std::vector<double> ma_outputs_vector;
	std::vector<double> ma_inputs_vector;
	// publisher and subscriber
	private: ros::Subscriber subJoystickMA;
	private: ros::Subscriber subSensors_pc;
	private: ros::Subscriber subPlanned_path;
    private: ros::Publisher pubMAState;	
	private: ros::Publisher pubMASensors;
	 private: ros::Publisher pubMASensors_brake;
// ros stuff
    private: ros::NodeHandle rosNode;
    private: ros::CallbackQueue queue;
    private: boost::thread callbackQueueThread;
    private: double rosPublishPeriod;
   // private: ros::Time lastRosPublishTime;
    private:  microautobox::maudp  client_ma;
    private:   ros::Timer timer;
    private:   double lastcall;
	

void QueueThread(){
  static const double timeout = 0.001;

  while (this->rosNode.ok())
  {
    this->queue.callOne(ros::WallDuration(timeout));
  }
};
/**
this function multiplex the vector that sent the MA to the topic that is required
@param type_state  the type of message received by the MA
@param time the timestamp time to be added to the messages
**/
void process_inputs(const unsigned short &type_state,const double &time){
					switch(type_state){ //TODO:will be better to add a header to each message? how to make this? there is not an standard msg array with header
					case 1000:
					sensors_ma.data[0]=time;
					std::copy ( ma_outputs_vector.begin(), ma_outputs_vector.end(), sensors_ma.data.begin()+1);
					this->pubMASensors.publish(sensors_ma);
					break;
					case 1100:
					sensors_brake.data[0]=time;
					std::copy ( ma_outputs_vector.begin(), ma_outputs_vector.end(), sensors_brake.data.begin()+1);
					this->pubMASensors_brake.publish(sensors_brake);
					break;
					case 2000:
					states_ma.data[0]=time;
					std::copy ( ma_outputs_vector.begin(), ma_outputs_vector.end(), states_ma.data.begin()+1);
					this->pubMAState.publish(states_ma);
					break;
					
				}
}
/**
this function multiplex the vector to send to the MA from the received(subscriber) topics
to multiplex, each suscriber topic has a queue, and this queue is pop in the next priority:
first the sensors_pc topic and sent over type_comando=200 , if there is nothing there , 
second it look for data from joystick_ma topics and sent over type_comando=30 
third it look for data from planned_path topics and sent over type_comando=path_planned.data[0],
finally, if all the queue are empty it will sent a commodity message type_comando=0, just to receive the data of the MA

**/
unsigned short process_outputs(){
		unsigned short type_comando=0;
		if(!sensors_pc_queue.empty()){
			sensors_pc=sensors_pc_queue.front();
			type_comando=200;
			std::copy ( sensors_pc.data.begin(), sensors_pc.data.end(), ma_inputs_vector.begin());
			//std::fill(ma_inputs_vector.begin()+6,ma_inputs_vector.end(), 0.0);
			sensors_pc_queue.pop();
				
		}
		else{
		if(!joystick_ma_queue.empty()){
			joystick_ma=joystick_ma_queue.front();
			type_comando=30;
			std::copy ( joystick_ma.data.begin(), joystick_ma.data.end(), ma_inputs_vector.begin());
			//std::fill(ma_inputs_vector.begin()+6,ma_inputs_vector.end(), 0.0);
			joystick_ma_queue.pop();
			operation_mode=3;
		}
		else{
		if(!planned_path_queue.empty()){
			planned_path=planned_path_queue.front();
///@bug By the security it is necessary to verify that planned_path.data[0] are in the permitted type of messages to sent
			type_comando=(unsigned int )(planned_path.data[0]);
			std::copy ( planned_path.data.begin()+1, planned_path.data.end(), ma_inputs_vector.begin());
			//std::fill(ma_inputs_vector.begin()+6,ma_inputs_vector.end(), 0.0);
			planned_path_queue.pop();
			operation_mode=4;		
		}
		else{
		///@TODO load data received from global_path_ma, local_path and state of the car, first send the global_path and then start the mode ??
		/// it is better by default send the sate of the car or just when is in control_path_mode
		ma_inputs_vector[0]=operation_mode;
		ma_inputs_vector[1]=lastcall;
		ma_inputs_vector[2]=152;
		std::fill(ma_inputs_vector.begin()+3,ma_inputs_vector.end(), 0.0);
		}
		}
		}
return type_comando;

}
/** 
this function process the event when joystick_ma topic is received, saving there in a queue
@param _msg the message joystick_ma received
*/
void SetJoystickMA(const std_msgs::Float64MultiArray::ConstPtr &_msg){
	std_msgs::Float64MultiArray tmp;
	tmp.data=_msg->data;
	joystick_ma_queue.push(tmp);
};
/** 
this function process the event when sensors_pc topic is received, saving there in a queue
@param _msg the message sensors_pc received
*/
void SetSensorsPC(const std_msgs::Float64MultiArray::ConstPtr &_msg){
	std_msgs::Float64MultiArray tmp;
	tmp.data=_msg->data;
	//for(int i=0;i<_msg->data.size();i++)
	//	std::cout<<_msg->data[i]<<",";
	//std::cout<<"\n";
	sensors_pc_queue.push(tmp);
};

/** 
this function process the event when planned_path topic is received, saving there in a queue
@param _msg the message planned_pc received
*/
void SetPlanned_path(const std_msgs::Float64MultiArray::ConstPtr &_msg){
	std_msgs::Float64MultiArray tmp;
	tmp.data=_msg->data;
	//for(int i=0;i<_msg->data.size();i++)
	//	std::cout<<_msg->data[i]<<",";
	//std::cout<<"\n";
	planned_path_queue.push(tmp);
};

/**
 @brief This is the timer event that send and receive data from the MA
@param t    the timer data ros::TimerEvent
*/
private: void RosPublishStatesMA(const ros::TimerEvent& t){
		double al=t.current_real.toSec()-lastcall;
		//ROS_INFO("Requ");
  // if(al>this->rosPublishPeriod/2)
	//{
		lastcall=t.current_real.toSec();
		//std::fill(ma_inputs_vector.begin(),ma_inputs_vector.end(), 0.0);
		unsigned short type_comando=process_outputs();
		unsigned short type_state=32768;
		//double time_1=ma_outputs_vector.data[1]; como sei que e o valor?
		if(timercounter==0){//{/client_ma.is_open()){
		std::string a=client_ma.ma_udp_request(&ma_outputs_vector[0],&type_state,&ma_inputs_vector[0],type_comando);
		//std::string a;
		if(!a.empty()){
			if (client_ma.get_udp_error()<12 ) //print the first 10 errors
				ROS_ERROR("%s",a.c_str());
				
			if(client_ma.get_udp_error()>1000){
				//ROS_ERROR("%s",client_ma.close_udp_socket().c_str());
				timercounter=1;
				ROS_ERROR("i will try to reconnect in 1000 Ts ");
			}

				
		}

		else{

			process_inputs(type_state,t.current_real.toSec());
				

			if(al>1.5*rosPublishPeriod || al<0.5*rosPublishPeriod){
				ROS_INFO("MA delay %f ||| %f || %f",al,t.current_real.toSec(),t.current_expected.toSec());
			}
			
		}
		}
		else
		{
			;
			timercounter++;
			if (timercounter>1000){ //dos minutos
				if(client_ma.open_udp_socket())
					ROS_INFO("Connected again to MA");
				else
					ROS_ERROR("Error to open the socket");
			timercounter=0;
			}
		}
		
		//sensors_ma.data[1]=lastcall;
		//set data received from ma in states_ma

	/*}

/*if(al<this->rosPublishPeriod/2)
	ROS_INFO("period very short %f ||| %f",al,t.profile.last_duration.toSec());
if(al>this->rosPublishPeriod*1.5)
	ROS_INFO("period very long %f ||| %f",al,t.profile.last_duration.toSec());*/
		
	};

	
/// Returns the ROS publish period (seconds).
public: double GetRosPublishPeriod(){
  return this->rosPublishPeriod;
	};
public:	
/**
 @brief construtor of the class. it configure each publisher and suscriber and give priority to the thread of the Timer
*/
vilma_ma_ros():rosNode("vilma_ma_ros")
{
  // ros stuff
  //->rosNode = new ros::NodeHandle("vilma_ma_ros");
  timercounter=0;
  operation_mode=2;
  int port, ma_port, timeout;
  std::string ma_ip;

	this->rosNode.param("microautobox/udp_port", port, 5001);
	this->rosNode.param("microautobox/ma_port", ma_port, 5001);
	this->rosNode.param<std::string>("microautobox/ma_ip", ma_ip, "192.168.140.3");
	this->rosNode.param("microautobox/timeout", timeout, 4);
	this->rosNode.param("microautobox/Ts", this->rosPublishPeriod, 0.01);
	this->rosNode.param("microautobox/ma_inputs", this->ma_inputs, 10);
	this->rosNode.param("microautobox/ma_outputs", this->ma_outputs, 30);
	joystick_ma.data.reserve(ma_inputs);
	joystick_ma.data.resize(ma_inputs,0.0);
	ma_inputs_vector.reserve(ma_inputs);
	ma_inputs_vector.resize(ma_inputs,0.0);
	ma_outputs_vector.reserve(ma_outputs);
	ma_outputs_vector.resize(ma_outputs,0.0);
	
	sensors_ma.data.reserve(ma_outputs+1);
	sensors_ma.data.resize(ma_outputs+1,0.0);
	sensors_brake.data.reserve(ma_outputs+1);
	sensors_brake.data.resize(ma_outputs+1,0.0);
	states_ma.data.reserve(ma_outputs+1);
	states_ma.data.resize(ma_outputs+1,0.0);
	
//	sensors_ma.layout.dim[0].size = ma_outputs+1;
	//sensors_ma.layout.dim[0].stride = ma_outputs+1;
//	sensors_ma.layout.dim[0].label = "MA outputs";



if(!client_ma.configure(port,ma_port,ma_ip,boost::posix_time::millisec(timeout),ma_inputs,ma_outputs))
    ROS_ERROR("the udp port in the pc is not accessible");

ros::SubscribeOptions joystick_ma_so =
      ros::SubscribeOptions::create<std_msgs::Float64MultiArray>(
      "joystick_ma", 10,
      boost::bind(static_cast< void (vilma_ma_ros::*)
        (const std_msgs::Float64MultiArray::ConstPtr&) >(&vilma_ma_ros::SetJoystickMA), this, _1),
      ros::VoidPtr(),  NULL);//&this->queue);
      
	   this->subJoystickMA = this->rosNode.subscribe(joystick_ma_so);

ros::SubscribeOptions sensors_pc_so =
      ros::SubscribeOptions::create<std_msgs::Float64MultiArray>(
      "sensors_pc", 1,
      boost::bind(static_cast< void (vilma_ma_ros::*)
        (const std_msgs::Float64MultiArray::ConstPtr&) >(&vilma_ma_ros::SetSensorsPC), this, _1),
      ros::VoidPtr(),  NULL);//&this->queue);
	   this->subSensors_pc = this->rosNode.subscribe(sensors_pc_so);

ros::SubscribeOptions planned_path_so =
      ros::SubscribeOptions::create<std_msgs::Float64MultiArray>(
      "planned_path", 1,
      boost::bind(static_cast< void (vilma_ma_ros::*)
        (const std_msgs::Float64MultiArray::ConstPtr&) >(&vilma_ma_ros::SetPlanned_path), this, _1),
      ros::VoidPtr(),  NULL);//&this->queue);
	   this->subPlanned_path = this->rosNode.subscribe(planned_path_so);


	   this->pubMAState = this->rosNode.advertise<std_msgs::Float64MultiArray>("state_ma", 2);
	   this->pubMASensors = this->rosNode.advertise<std_msgs::Float64MultiArray>("sensors_ma", 1);
	   this->pubMASensors_brake = this->rosNode.advertise<std_msgs::Float64MultiArray>("sensors_brake", 1);
	   	lastcall=ros::Time::now().toSec();

	
//the timer will be in other thread different that the other callback		
//	ros::WallTimerOptions timer_op=ros::WallTimerOptions(ros::WallDuration(this->rosPublishPeriod),boost::bind(&vilma_ma_ros::RosPublishStatesMA,this,_1),&this->queue);
//	timer=this->rosNode.createTimer(timer_op);

	ros::TimerOptions timer_op=ros::TimerOptions(ros::Duration(this->rosPublishPeriod),boost::bind(&vilma_ma_ros::RosPublishStatesMA,this,_1),&this->queue);
	timer=this->rosNode.createTimer(timer_op);
    // ros callback queue for processing subscription
    this->callbackQueueThread = boost::thread(boost::bind(&vilma_ma_ros::QueueThread, this));

	
	//define the thread priority for the sensors
#if defined(BOOST_THREAD_PLATFORM_PTHREAD)
  int pid=getppid();
  
   if(setpriority(PRIO_PROCESS, 0, -15)==-1)
	   ROS_INFO("priority of the proccess can not be set(may with renice) pidof: %d ",pid);
   
	pthread_t threadID = (pthread_t) callbackQueueThread.native_handle();
	int priority=99;
	struct sched_param param;
	param.sched_priority = priority;	
	pthread_attr_t attr;
	int  inheritsched=PTHREAD_EXPLICIT_SCHED; //PTHREAD_INHERIT_SCHED
	pthread_attr_setinheritsched(&attr, inheritsched);

	//pthread_attr_init(&attr);
	
	if(pthread_setschedparam( threadID, SCHED_RR,&param)!=0 )
		ROS_INFO("scheduling of the threads can not be set %d ",(int)threadID); //SCHED_RR//SCHED_FIFO//SCHED_OTHER
#endif
	   
};
    /// \brief Destructor.
    public: virtual ~vilma_ma_ros(){
		std::string tsg=this->client_ma.close_udp_socket();
		//ROS_INFO(tsg);
		this->rosNode.shutdown();
		this->queue.clear();
		this->queue.disable();
		this->callbackQueueThread.join();
		// lete this->rosNode;
	};
};
/**
*@brief This is main program of the package VILMA_MA
*/
int main(int argc, char **argv)
{
  ros::init(argc, argv, "vilma_ma_ros_node");

  vilma_ma_ros vilma_node;
  ros::spin();
  return 0;
}
